import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.plivo.helper.exception.PlivoException;
import com.plivo.helper.xml.elements.Message;
import com.plivo.helper.xml.elements.PlivoResponse;

public class MobileVoting extends HttpServlet {

    protected static ArrayList<String> VoterTable = new ArrayList<>();
    protected static ArrayList<Candidate> TallyTable = new ArrayList<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try (PrintWriter writer = response.getWriter()) {

            writer.println("<!DOCTYPE html><html>");
            writer.println("<head>");
            writer.println("<meta charset=\"UTF-8\" />");
            writer.println("<title>CSDAY 2016 Voting</title>");
            writer.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n");
            writer.println("</head>");
            writer.println("<body>");
            writer.println("<h1>CSDay 2016 Scoreboard</h1>");

            SMSReport webReport = new SMSReport(10, TallyTable);
            writer.println(webReport.toHTML());

            writer.println("<br /><div><input class=\"btn\" type=\"button\" value=\"Refresh Votes\" onClick=\"window.location.reload()\"></div>");

            writer.println("</body>");
            writer.println("</html>");
        }
    }

    /**
     * Translates incoming SMS messages into their desired operations.
     * @param request The XML message that Plivo sends
     * @param response The XML response we send back to Plivo
     */
    protected static void handleSMS (HttpServletRequest request, HttpServletResponse response) {
        // Start the result as an error message and adjust as needed.
        String result = "Sorry, I didn't understand that.\nTo cast a vote, simply respond with the number of the poster.";

        try {
            // Parse phone number and message
            String fromNumber = request.getParameter("From");
            String toNumber = request.getParameter("To");
            String msg[] = request.getParameter("Text").split(" ");

            // Determine the operation
            if (msg.length == 1) {
                result = castVote(msg[0], fromNumber);
            }

            // More than one parameter
            else {
                switch(msg[0].toUpperCase()) {
                    case "INIT":
                        if (msg.length == 3) {
                            result = initTable(msg[1], msg[2]);
                        } else {
                            result = "You've entered an invalid number of parameters to initialize the tables.";
                        }
                        break;
                    case "REPORT":
                        if (msg.length == 3) {
                            try {
                                int n = Integer.parseInt(msg[2]);
                                result = generateReport(msg[1], n);
                            } catch (Exception e) {
                                result = "You've entered an invalid number of posters to generate a report on.";
                            }
                        } else {
                            result = "You've entered an invalid number of parameters to generate a report.";
                        }
                        break;
                    default:
                        result = "Sorry, I didn't understand that.\nTo cast a vote, simply respond with the number of the poster.";
                        break;
                }
            }

            // Result string should now be set. Create the response.
            PlivoResponse plivoResponse = new PlivoResponse();
            Message plivoMsg = new Message(result);
            plivoMsg.setSource(toNumber);
            plivoMsg.setDestination(fromNumber);
            try {
                // And send it back to plivo for magical processing.
                plivoResponse.append(plivoMsg);
                System.out.println("IN:\t" + fromNumber + " " + request.getParameter("Text"));
                System.out.println("OUT:\t" + plivoResponse.toXML());
                response.addHeader("Content-Type", "text/xml");
                response.getWriter().print(plivoResponse.toXML());
            } catch (PlivoException e) {
                e.printStackTrace();
            }

            // DEBUG
            //System.out.println("Message Received");
            //System.out.println("\t" + fromNumber + ": " + request.getParameter("Text"));
            //System.out.println("Response:");
            //System.out.println("\t" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Creates a new report of the current top N posters.
     * @param pw password
     * @param n number of posters to show
     * @return Formatted string of a report, or failure.
     */
    protected static String generateReport(String pw, int n) {
        // Check password
        if (!checkPass(pw)) {
            return "You've entered an invalid password";
        }

        // Good password
        else {
            SMSReport report = new SMSReport(n, TallyTable);
            return report.toString();
        }
    }

    /**
     * Initializes a table of candidates to be voted on. Destroys current voter/tally tables.
     * @param pw password to be verified to ensure valid admin
     * @param candidates semicolon delimited list of candidates (ints)
     * @return A formatted string of success or failure.
     */
    protected static String initTable(String pw, String candidates) {
        // Check the password.
        if (!checkPass(pw)) {
            return "You've entered an invalid password.";
        }

        // Valid password
        else {

            // Create a new tally table
            ArrayList<Candidate> newTallyTable = new ArrayList<>();

            try {
                // Separate the list of delimited candidates into a new list.
                List<String> candidateList = Arrays.asList(candidates.split(";"));
                // Place all the new candidates into our Tally Table.
                Candidate currentCandidate;
                for (String cur : candidateList) {
                    currentCandidate = new Candidate(Integer.parseInt(cur));
                    newTallyTable.add(currentCandidate);
                }

                // Replace the tally tables, and nuke the voter table.
                TallyTable = newTallyTable;
                VoterTable = new ArrayList<>();
                return "Candidate table has been initialized successfully.";

            } catch (Exception e) {
                //e.printStackTrace();
                return "There was something wrong with your list of candidates.";
            }
        }
    }

    protected static boolean checkPass(String pw) {
        String password = "secret";
        return pw.equalsIgnoreCase(password);
    }

    /**
     * Verifies the validity of a vote and then casts it.
     * @param vote the incoming vote
     * @param phoneNumber the phone number source
     * @return A string indicating success or failure to be sent back to the voter.
     */
    protected static String castVote(String vote, String phoneNumber) {

        // Check if this user has already voted.
        if (VoterTable.contains(phoneNumber)) {
            return "You have already cast a vote.";
        }

        int candidateID;

        // Not a repeat voter. Determine if the candidate is a valid number.
        try {
            candidateID = Integer.parseInt(vote);
        } catch (Exception e) {
            e.printStackTrace();
            return "You've entered an invalid candidate.";
        }

        // Determine if the candidate ID they're voting for is valid
        boolean match = false;
        Candidate cand = null;
        if (TallyTable.size() > 0) {
            for (Candidate cur : TallyTable) {
                if (cur.getID() == candidateID && (!match)) {
                    match = true;
                    cand = cur;
                }
            }
        }

        // The candidate exists, update the voter and tally tables.
        if (match) {
            cand.addVote();
            VoterTable.add(phoneNumber);
            return "Your vote for poster " + candidateID + " has been cast!";
        }

        // The candidate doesn't exist.
        else {
            return "You've entered an invalid candidate.";
        }

    }

}
