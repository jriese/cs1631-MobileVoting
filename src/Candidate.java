import java.util.Comparator;

public class Candidate {
    public int id;
    public int votes;
    public Candidate (int incID) {
        id = incID;
        votes = 0;
    }

    public int getID() {
        return id;
    }

    public int getVotes() {
        return votes;
    }

    public void addVote() {
        votes++;
    }
}

class CandidateComparator implements Comparator<Candidate> {
    @Override
    public int compare(Candidate o1, Candidate o2) {
        return o2.getVotes() - o1.getVotes();
    }
}