import com.plivo.helper.exception.PlivoException;
import com.plivo.helper.xml.elements.PlivoResponse;
import com.plivo.helper.xml.elements.Speak;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class VoiceVote extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String digit = request.getParameter("Digits");
        String fromNumber = request.getParameter("From");
        PlivoResponse presponse = new PlivoResponse();
        Speak spk = new Speak(MobileVoting.castVote(digit, fromNumber));
        try {
            presponse.append(spk);
        } catch (PlivoException e) {
            e.printStackTrace();
        }

        System.out.println(presponse.toXML());
        response.addHeader("Content-Type", "text/xml");
        response.getWriter().print(presponse.toXML());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
