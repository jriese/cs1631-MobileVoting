import java.util.ArrayList;
import java.util.Collections;

public class SMSReport {
    private final ArrayList<Candidate> topCandidates = new ArrayList<>();

    public SMSReport(int s, ArrayList<Candidate> cand) {

        if (cand == null) {
            s = 0;
        }
        else if (cand.size() < s) {
            s = cand.size();
        }

        if (!(cand == null)) {
            Collections.sort(cand, new CandidateComparator());
            for (int i = 0; i < s; i++) {
                topCandidates.add(cand.get(i));
            }
        }
    }

    // Format our report
    @Override
    public String toString() {
        String out = "Top Posters:";
        for (int i = 0; i < topCandidates.size(); i++) {
            Candidate cur = topCandidates.get(i);
            out += "\n#"+(i+1)+": Poster " + cur.getID() + " has " + cur.getVotes() + " votes.";
        }
        return out;
    }

    public String toHTML() {
        String out = "<table class=\"reporttable\"><thead><tr><th>Rank</th><th>Poster Name</th><th># of Votes</th></tr></thead><tbody>";
        for (int i = 0; i < topCandidates.size(); i++) {
            Candidate cur = topCandidates.get(i);

            out += "<tr><td>" + (i+1) + "</td><td>Poster " + cur.getID() + "</td><td>" + cur.getVotes() + "</td></tr>";
        }

        out += "</tbody></table>";
        return out;
    }

    public String toVoice() {
        Candidate cur = topCandidates.get(0);
        String out = "The current top poster is poster " + cur.getID() + ", with "  + cur.getVotes() + " votes.";
        cur = topCandidates.get(1);
        out += " The second place poster is poster " + cur.getID() + ", with " + cur.getVotes() + " votes.";
        return out;
    }
}
