import com.plivo.helper.exception.PlivoException;
import com.plivo.helper.xml.elements.GetDigits;
import com.plivo.helper.xml.elements.PlivoResponse;
import com.plivo.helper.xml.elements.Speak;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class GetVoice extends HttpServlet {
    final String MENU_MESSAGE = "Welcome to CS Day Voting. Press 1 to cast a vote. Press 2 to list the current leading posters. Press 3 to get a list of posters you can vote for.";
    final String ERROR_MESSAGE = "Sorry, I didn't understand that. Please hang up and try again.";
    final String VOTE_MESSAGE = "Please enter the 1 or 2 digit number of the poster you wish to vote for.";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PlivoResponse presponse = new PlivoResponse();
        GetDigits gd = new GetDigits();
        gd.setAction("https://dev.jriese.com/mobilevoting/voicemenu");
        gd.setMethod("POST");
        gd.setNumDigits(1);
        gd.setTimeout(7);
        gd.setRetries(1);
        Speak spk = new Speak(MENU_MESSAGE);
        Speak speak = new Speak(ERROR_MESSAGE);
        try {
            gd.append(spk);
            presponse.append(gd);
            presponse.append(speak);
            System.out.println(presponse.toXML());
            response.addHeader("Content-Type", "text/xml");
            response.getWriter().print(presponse.toXML());;
        } catch (PlivoException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String digit = request.getParameter("Digits");
        PlivoResponse presponse = new PlivoResponse();

        if (digit.equals("1"))
        {
            // Read out a text
            GetDigits gd = new GetDigits();
            gd.setAction("https://dev.jriese.com/mobilevoting/voicevote");
            gd.setMethod("POST");
            gd.setNumDigits(2);
            gd.setTimeout(7);
            gd.setRetries(1);
            Speak spk = new Speak(VOTE_MESSAGE);
            try {
                gd.append(spk);
                presponse.append(gd);
            } catch (PlivoException e) {
                e.printStackTrace();
            }
        }
        else if (digit.equals("2"))
        {
            // Generate a report and then play it back
            SMSReport report = new SMSReport(2, MobileVoting.TallyTable);
            Speak spk = new Speak(report.toVoice());
            try {
                presponse.append(spk);
            } catch (PlivoException e) {
                e.printStackTrace();
            }
        }
        else if (digit.equals("3"))
        {
            String list = "You can vote for the following posters: ";
            // Generate a list of valid vote targets
            for (Candidate cur : MobileVoting.TallyTable) {
                list += ", " + cur.getID();
            }
            Speak spk = new Speak(list);
            try {
                presponse.append(spk);
            } catch (PlivoException e) {
                e.printStackTrace();
            }
        }
        else
        {
            // Wrong input
            Speak speak = new Speak(ERROR_MESSAGE);
            try {
                presponse.append(speak);
            } catch (PlivoException e) {
                e.printStackTrace();
            }
        }

        System.out.println(presponse.toXML());
        response.addHeader("Content-Type", "text/xml");
        response.getWriter().print(presponse.toXML());
    }
}
