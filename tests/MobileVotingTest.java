import org.junit.*;
import org.junit.runners.MethodSorters;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MobileVotingTest {

    // Basic necessities
    private static MobileVoting mv;
    private static GetSMS gs;
    private MockHttpServletRequest req = new MockHttpServletRequest();
    private MockHttpServletResponse res;
    private SMSReport smsReport;

    // Some more necessary strings
    private String prefix;
    private int reportSize;
    private final String suffix = "</Message></Response>";
    private final String serverPhoneNo = "+14122750654";

    // Possible string responses
    private final String invalidNoParametersInitTally = "You've entered an invalid number of parameters to initialize the tables.";
    private final String invalidNoPostersGenReport =  "You've entered an invalid number of posters to generate a report on.";
    private final String invalidNoParametersGenReport = "You've entered an invalid number of parameters to generate a report.";
    private final String didntUnderstandThat = "Sorry, I didn't understand that.\nTo cast a vote, simply respond with the number of the poster.";
    private final String invalidPassword = "You've entered an invalid password.";
    private final String candidateTableInitialized = "Candidate table has been initialized successfully.";
    private final String invalidCandidateInitTable = "There was something wrong with your list of candidates.";
    private final String alreadyVoted = "You have already cast a vote.";
    private final String invalidCandidateVote = "You've entered an invalid candidate.";
    private final String voteSuccessPrefix = "Your vote for poster ";
    private final String voteSuccessSuffix = " has been cast!";
    private String successfulVote;  // generated on the fly by buildStrings()
    private String aValidReport;    // generated on the fly by buildStrings()


    @BeforeClass
    public static void oneTimeSetup() throws Exception {
        mv = new MobileVoting();
        gs = new GetSMS();
    }

    @Before
    public void setUp() throws Exception {
        resetRequests();
    }

    /**
     * Creates a new instance of the mock request and responses for performing a new test.
     */
    private void resetRequests() {
        req = new MockHttpServletRequest();
        res = new MockHttpServletResponse();
        reportSize = -1;
        aValidReport = "";
    }

    /**
     * Sets the requests "to" phone number.
     * @param phoneNo the phone number
     */
    private void setTo(String phoneNo) {
        req.addParameter("To", phoneNo);
    }

    /**
     * Sets the "from" phone number.
     * @param phoneNo the phone number
     */
    private void setFrom(String phoneNo) {
        req.addParameter("From", phoneNo);
    }

    /**
     * Sets the body of the text.
     * @param body body of the text baby
     */
    private void setText(String body) {
        req.addParameter("Text", body);
    }

    /**
     * Posts the mock request for handling.
     * @throws ServletException
     * @throws IOException
     */
    private void post() throws ServletException, IOException{
        gs.doPost(req,res);
        buildString();
    }

    /**
     * Creates the prefix/suffix messages for simpler formatting.
     */
    private void buildString() {
        prefix = "<Response><Message dst=\"" + req.getParameter("From") + "\" src=\"" + req.getParameter("To") + "\">";
        successfulVote = voteSuccessPrefix + req.getParameter("Text") + voteSuccessSuffix;
        if (reportSize >= 0) {
            smsReport = new SMSReport(reportSize, mv.TallyTable);
            aValidReport = smsReport.toString();
        }
    }

    /**
     * Simple wrapper for performing asserts, making life easy.
     * @param expectedResponse string response we expect to get back from the server
     * @throws UnsupportedEncodingException
     */
    private void doAssert(String expectedResponse) throws UnsupportedEncodingException {
        assertEquals(prefix + expectedResponse + suffix, res.getContentAsString());
    }

    @Test
    public void aValidInitTable() throws IOException, ServletException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("Init secret 1;2;3");
        post();
        doAssert(candidateTableInitialized);
    }

    @Test
    public void bValidInitialVote() throws IOException, ServletException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("1");
        post();
        doAssert(successfulVote);
    }

    @Test
    public void cDuplicateVote() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("2");
        post();
        doAssert(alreadyVoted);
    }

    @Test
    public void dInvalidNumberParametersInit() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("init secret 1;2 3 asdf");
        post();
        doAssert(invalidNoParametersInitTally);
    }

    @Test
    public void eInvalidCandidatesInitTally() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("init secret qbc");
        post();
        doAssert(invalidCandidateInitTable);
    }

    @Test
    public void fInvalidPasswordInit() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("init butts qbc");
        post();
        doAssert(invalidPassword);
    }

    @Test
    public void gBogusRequest() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("hey there");
        post();
        doAssert(didntUnderstandThat);
    }

    @Test
    public void hGoodTableInit() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("init secret 1;2;3;4;5");
        post();
        doAssert(candidateTableInitialized);
    }

    @Test
    public void iShouldBeAbleToVoteAgain() throws ServletException, IOException {
        resetRequests();
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("2");
        post();
        doAssert(successfulVote);
    }

    @Test
    public void jInvalidNumberOfPostersToReport() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("report secret x");
        post();
        doAssert(invalidNoPostersGenReport);
    }

    @Test
    public void kInvalidNumberParametersReport() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("report secret 4 5 asdf");
        post();
        doAssert(invalidNoParametersGenReport);
    }

    @Test
    public void lDuplicateVoterAgain() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("3");
        post();
        doAssert(alreadyVoted);
    }

    @Test
    public void mInvalidCandidateVote() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954142");
        setText("6");
        post();
        doAssert(invalidCandidateVote);
    }

    @Test
    public void nGenerateAReport() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12159954141");
        setText("report secret 3");
        reportSize = 3;
        post();
        doAssert(aValidReport);
    }

    @Test
    public void oGenerateAnotherReport() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12345");
        setText("report secret 5");
        reportSize = 5;
        post();
        doAssert(aValidReport);
    }

    @Test
    public void pGenerateAReportTooLarge() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12345");
        setText("report secret 25");
        reportSize = 25;
        post();
        doAssert(aValidReport);
    }

    @Test
    public void qGenerateAnEmptyReport() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12345");
        setText("report secret 0");
        reportSize = 0;
        post();
        doAssert(aValidReport);
    }

    @Test
    public void rReinitializeTables() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12345");
        setText("init secret 1;2;3;4;5;6;7;8");
        post();
        doAssert(candidateTableInitialized);
    }

    @Test
    public void sVoteFor1() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+12345");
        setText("1");
        post();
        doAssert(successfulVote);
    }

    @Test
    public void tVoteFor1() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+123456");
        setText("1");
        post();
        doAssert(successfulVote);
    }

    @Test
    public void uVoteFor1() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+1234567");
        setText("1");
        post();
        doAssert(successfulVote);
    }

    @Test
    public void vEnsure3TalliedVotesFor1() {
        assertEquals(mv.TallyTable.get(0).getVotes(), 3);
        assertEquals(mv.TallyTable.get(0).getID(), 1);
    }

    @Test
    public void wEnsure0TalliedVotesForOther() {
        assertEquals(mv.TallyTable.get(1).getVotes(), 0);
    }

    @Test
    public void xVoteFor2() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+18005882300");
        setText("2");
        post();
        doAssert(successfulVote);
    }

    @Test
    public void yEnsure1VoteFor2() {
        assertEquals(mv.TallyTable.get(1).getVotes(), 1);
        assertEquals(mv.TallyTable.get(1).getID(), 2);
    }

    @Test
    public void zGenerateOneLastReport() throws ServletException, IOException {
        setTo(serverPhoneNo);
        setFrom("+1234");
        setText("report secret 4");
        reportSize = 4;
        post();
        doAssert(aValidReport);
    }
}
